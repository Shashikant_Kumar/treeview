﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TreeViewControlUsed
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Add("People");
            treeView1.Nodes.Add("Animal");
            treeView1.Nodes[0].Nodes.Add("SK");
            treeView1.Nodes[0].Nodes.Add("VK");
            treeView1.Nodes[0].Nodes.Add("Sanjay");
            treeView1.Nodes[1].Nodes.Add("Dog");
            treeView1.Nodes[1].Nodes.Add("Cat");
            treeView1.Nodes[1].Nodes[0].Nodes.Add("Spot");
            treeView1.Nodes[1].Nodes[0].Nodes[0].Nodes.Add("Scategory");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //treeView1.Nodes.Clear();
            ChechkedNodeRemove(treeView1.Nodes);
        }
        List<TreeNode> tnlist = new List<TreeNode>();

        void ChechkedNodeRemove(TreeNodeCollection tnc)
        {
            foreach (TreeNode  tn in tnc)
            if (tn.Checked)
                    tnlist.Add(tn);
                else if (tn.Nodes.Count != 0)
                    ChechkedNodeRemove(tn.Nodes);
            foreach (TreeNode n in tnlist)
                treeView1.Nodes.Remove(n);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TreeNode tn = new TreeNode();
            tn.ImageIndex = 0;
            tn.Text = "Person";
            treeView1.Nodes.Add(tn);
            TreeNode tn1 = new TreeNode();
            tn1.ImageIndex = 1;
            tn1.SelectedImageIndex = 1;
            tn1.Text = "Animal";
            treeView1.Nodes.Add(tn1);
            TreeNode tn2 = new TreeNode();
            tn2.ImageIndex = 2;
            tn2.SelectedImageIndex = 2;
            tn2.Text = "cat";
            treeView1.Nodes[1].Nodes.Add(tn2);

        }
    }
}
